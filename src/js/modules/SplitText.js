// Helper vars and functions.
var extend = function (a, b) {
    for (var key in b) {
        if (b.hasOwnProperty(key)) {
            a[key] = b[key];
        }
    }
    return a;
}

function SplitText(el, options) {
    this.el = el;
    this.options = extend({}, this.options);
    extend(this.options, options);
    this.chars = [];
    this.charElements = [];
    this.holder = null;
    this._layout();
}

/**
 * necessary structure
 */
SplitText.prototype._layout = function () {
    var that = this;
    this.holder = document.createElement("div");
    this.holder.setAttribute("class", "textEffects");
    this.el.parentNode.appendChild(this.holder);


    // You could split the string on the whitespace and then re - add it, since you know its in between every one of the entries.
    var string = $(this.el).text();
    string = string.split("");
    this.chars = new Array();
    for (var i = 0; i < string.length; i++) {
        this.chars.push(string[i]);
    }

    this.chars.forEach(function(e) {
        var cEl = document.createElement("span");
        var textNode = document.createTextNode(e);
        cEl.appendChild(textNode);
        that.holder.appendChild(cEl);
        that.charElements.push(cEl);
    })
}


// window.SplitText = SplitText;
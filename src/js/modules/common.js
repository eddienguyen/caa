// Không cần onload ở đây nữa, lúc này common.js đã chạy sau window.onload rồi
console.log("I'm common JS! I'm ready now!");
var sw, sh;

$('.js_toggleDropdown').on('click', function (e) {
    e.preventDefault();
    e.stopPropagation();
    if (!$(this).hasClass('openSelect')) {
        $(this).addClass('openSelect');
    } else {
        $(this).removeClass('openSelect');
    }
});

//Optional parameter includeMargin is used when calculating outer dimensions  
(function ($) {
    $.fn.getHiddenDimensions = function (includeMargin) {
        var $item = this,
            props = { position: 'absolute', visibility: 'hidden', display: 'block' },
            dim = { width: 0, height: 0, innerWidth: 0, innerHeight: 0, outerWidth: 0, outerHeight: 0 },
            $hiddenParents = $item.parents().addBack().not(':visible'),
            includeMargin = (includeMargin == null) ? false : includeMargin;

        var oldProps = [];
        $hiddenParents.each(function () {
            var old = {};

            for (var name in props) {
                old[name] = this.style[name];
                this.style[name] = props[name];
            }

            oldProps.push(old);
        });

        dim.width = $item.width();
        dim.outerWidth = $item.outerWidth(includeMargin);
        dim.innerWidth = $item.innerWidth();
        dim.height = $item.height();
        dim.innerHeight = $item.innerHeight();
        dim.outerHeight = $item.outerHeight(includeMargin);

        $hiddenParents.each(function (i) {
            var old = oldProps[i];
            for (var name in props) {
                this.style[name] = old[name];
            }
        });

        return dim;
    }
}(jQuery));

var ANGKOR = {
    init: function () {
        GLoader.loadScripts([
            path_resource + "js/plugins/scrollmagic/ScrollMagic.min.js",
            path_resource + "js/plugins/scrollmagic/plugins/animation.gsap.min.js",
            path_resource + "js/plugins/scrollmagic/plugins/debug.addIndicators.min.js",

            path_resource + 'js/plugins/datetimepicker/jquery.datetimepicker.min.css',
            path_resource + 'js/plugins/datetimepicker/jquery.datetimepicker.full.min.js',

            path_resource + "js/modules/SplitText.js"
        ], function () {
            ANGKOR.sectionTitleAnimation();
            ANGKOR.bookingOnScroll();
            ANGKOR.bookingTabContent();
            ANGKOR.bookingDatePicker();
        });
        ANGKOR.bookingSelection();
        ANGKOR.recalculatePassengers();
        // ANGKOR.bookingDynamicInputPadding();

        // select origin && destination 
        // $("#booking .second select").each(function (index, element) {
        //     $(element).on("change", function (e) {
        //         // var originValue = $('#origin').find(":selected").text();
        //         // var destinationValue = $('#destination').find(":selected").text();

        //         var optionSelected = $("option:selected", this);
        //         var valueSelected = this.value;

        //         console.log("optionSelected: ", optionSelected);
        //         console.log("valueSelected: ", valueSelected);
        //     });
        // });

        $("#booking .formControl input").each(function (index, element) {
            var label = $(this).parent().find("label");
            $(element).on("change", function (e) {
                if (($(this).val() === "") || ($(this).val() == 0)) {
                    $(label).removeClass("withValueLabel").addClass("withoutValueLabel");
                } else {
                    $(label).removeClass("withoutValueLabel").addClass("withValueLabel");
                }
                // to.datepicker("option", "minDate", getDate(this));
                ANGKOR.recalculatePassengers();
            }).on("focus blur keyup", function (e) {
                $(label).removeClass("withoutValueLabel").addClass("withValueLabel");
            })
        });

        $(".js_increasePassengers").click(function (e) {
            // console.log($(this).parent().find("[type=number]"));
            $(this).parent().find('[type=number]').val(function (i, oldVal) {
                return ++oldVal;
            });
            $(this).parent().find('[type=number]').trigger("change");
            // $(label).removeClass("withoutValueLabel").addClass("withValueLabel");
        });

        $(".js_decreasePassengers").click(function (e) {
            // console.log($(this).parent().find("[type=number]"));
            $(this).parent().find('[type=number]').val(function (i, oldVal) {
                return Math.max(oldVal - 1, 0);
            });
            $(this).parent().find('[type=number]').trigger("change");
            // $(label).removeClass("withoutValueLabel").addClass("withValueLabel");
        });

        $(".js_toggleAdditionalOption").click(function (e) {
            e.preventDefault();
            e.stopPropagation();

            if ($(this).hasClass("hiding")) {
                $(this).removeClass("hiding");
                $(this).addClass("showing");
                $(".additionalOptions").removeClass("hide");
                $(".additionalOptions").addClass("show");
                $("#checkinreference").attr("disabled", true);
                $("#checkinreferencelastname").attr("disabled", true);
                $("#checkinreference").parent().addClass("disabled");
                $("#checkinreferencelastname").parent().addClass("disabled");

            } else {
                $(this).removeClass("showing");
                $(this).addClass("hiding");
                $(".additionalOptions").addClass("hide");
                $(".additionalOptions").removeClass("show");
                $("#checkinreference").attr("disabled", false);
                $("#checkinreferencelastname").attr("disabled", false);
                $("#checkinreference").parent().removeClass("disabled");
                $("#checkinreferencelastname").parent().removeClass("disabled");
            }

            $("#tab2").css("height", "auto");
        })
    },

    recalculatePassengers: function () {
        var totalElm = $("#passengers"),
            allPassengerInputs = $(".passengersInputControl2 [type=number]"),
            totalPassenger = 0;
        var totalLabel = totalElm.parent().find("label");
        allPassengerInputs.each(function (index, element) {
            totalPassenger = parseInt(totalPassenger) + parseInt($(this).val());
        });
        // console.log("totalPassenger : ", totalPassenger);
        totalElm.val(function (i, oldVal) {
            return totalPassenger;
        });
        // passensers input:
        if (totalPassenger > 0) {
            totalElm.removeClass("large huge").addClass("small");
            $(totalLabel).removeClass("withoutValueLabel").addClass("withValueLabel");
        }
        if (totalPassenger > 9) {
            totalElm.removeClass("small huge").addClass("large");
        }
        if (totalPassenger > 99) {
            totalElm.removeClass("large small").addClass("huge");
        }
    },
    bookingOnScroll: function () {
        var smController = new ScrollMagic.Controller(),
            bookingHeight = $("#booking").innerHeight(),
            bookingPosTop = $("#booking").offset().top + $("#booking").innerHeight(),
            isFixed = false,
            isOnBottom = false,
            $footerMain = $("footer .rowMain"),
            $footerContainer = $("footer > .container"),
            $toggler = $(".js_toggleBooking"),
            toggleBookingTL = new TimelineMax({ paused: true });

        TweenMax.set("#booking", {
            transformOrigin: "bottom right"
        })
        toggleBookingTL.to("#booking", 0.4, {
            // top: $toggler.offset().top,
            // left: $toggler.offset().left,
            // yPercent: 120,
            y: 70,
            autoAlpha: 0,
            // bottom: 0,
            onComplete: function () {
                toggleBookingTL.kill();
            }
        });

        // var tl = new TimelineMax({ paused: true });
        // tl
        //     .to("#booking", 0.5, {
        //         autoAlpha: 0,
        //         // y: window.innerHeight - bookingHeight,

        //         onComplete: function () {
        //             $('#booking').addClass('fixed');
        //             $("#booking").closest(".container").css({
        //                 paddingBottom: bookingHeight
        //             });
        //             // TweenMax.to("#booking", 0.3, { clearProps: "all", delay: 0.6 });
        //         }
        //     })
        //     .add('end1', 0.6)
        //     .to("#booking", 0.3, {
        //         autoAlpha: 1,
        //         onComplete: function () {
        //             TweenMax.killTweensOf(tl);
        //         }
        //     }, "end1");

        $(window).scroll(function (event) {
            var pos = $(this).scrollTop();
            if (pos > bookingPosTop) {
                if (!isFixed) {
                    isFixed = true;
                    $toggler.addClass("fixed");
                    TweenMax.to("#booking", 0.5, {
                        autoAlpha: 0,
                        y: 70,
                        onStart: function () { },
                        onComplete: function () {
                            $('#booking').addClass('fixed minimized');
                            // TweenMax.to("#booking", 0.7, {
                            //     autoAlpha: 1,
                            //     delay: 0.2,
                            //     ease: Sine.easeOut,
                            //     y: 0,
                            //     onStart: function () { },
                            // });
                        }
                    })
                }
            } else {
                if (isFixed) {
                    isFixed = false;
                    $('#booking').removeClass('fixed minimized');
                    TweenMax.to("#booking", 0.3, {
                        autoAlpha: 0,
                        y: 70,
                        onStart: function () {
                            $toggler.removeClass("fixed");
                        },
                        onComplete: function () {
                            TweenMax.to("#booking", 0.7, {
                                autoAlpha: 1,
                                delay: 0.2,
                                y: 0,
                                yPercent: 0,
                                ease: Sine.easeOut,
                                onComplete: function () { }
                            });
                        }
                    })
                }

            }
            // if ((window.innerHeight + window.scrollY) >= document.body.offsetHeight - $("footer .rowMain").height()) {
            //     if (!isOnBottom) {
            //         isOnBottom = true;
            //         $("footer > .container").css({
            //             paddingBottom: bookingHeight
            //         });
            //         TweenMax.to("#booking", 0.3, {
            //             bottom: "60px",
            //             ease: Power3.easeOut
            //         });
            //     }
            // } else {
            //     if (isOnBottom) {
            //     isOnBottom = false;
            //     $("footer > .container").css({
            //         paddingBottom: 0
            //     });
            //     TweenMax.to("#booking", 0.3, {
            //         bottom: "0",
            //         ease: Power3.easeOut
            //     });
            //     return;
            //     }
            // }
        });

        var bottomScene = new ScrollMagic.Scene({
            triggerElement: "footer .rowMain",
            triggerHook: 1,
            // reverse: true,
        })
            .on("enter", function (e) {
                TweenMax.to("#booking", 0.7, {
                    bottom: $footerMain.height(),
                    ease: Power3.easeOut,
                    onStart: function () {
                        $toggler.css({
                            bottom: $footerMain.height(),
                        })
                    },
                    onComplete: function () {
                        // if (!isOnBottom) {
                        isOnBottom = true;
                        // }
                    }
                });

            })
            .on("leave", function (e) {
                TweenMax.to("#booking", 0.7, {
                    bottom: "0",
                    ease: Power3.easeOut,
                    onStart: function () {
                        $toggler.css({
                            bottom: 0,
                        })
                    },
                    onComplete: function () {

                    }
                });
            })
            .addTo(smController);

        var bottomScene2 = new ScrollMagic.Scene({
            triggerElement: "footer",
            triggerHook: 1,
        })
            .on("enter", function (e) {
                if ($("#booking").hasClass("minimized")) {
                    $footerContainer.css({
                        paddingBottom: 0
                    });
                } else {
                    $footerContainer.css({
                        paddingBottom: bookingHeight
                    });
                }
            })
            .on("leave", function (e) {
                $footerContainer.css({
                    paddingBottom: 0
                });
            })
            .addTo(smController);

        $toggler.click(function (e) {
            e.preventDefault();
            if ($("#booking").hasClass("minimized")) {
                // toggleBookingTL.reverse();
                TweenMax.to("#booking", 0.4, {
                    y: 0,
                    autoAlpha: 1,
                    onComplete: function () {
                        $("#booking").removeClass("minimized");
                        $footerContainer.css({
                            paddingBottom: bookingHeight
                        });
                    }
                });

            } else {
                // toggleBookingTL.play();
                TweenMax.to("#booking", 0.4, {
                    y: 70,
                    autoAlpha: 0,
                    onComplete: function () {
                        $("#booking").addClass("minimized");
                        $footerContainer.css({
                            paddingBottom: 0
                        });
                    }
                });
            }
        });
    },
    bookingTabContent: function () {
        var $filterTabs = $(".js_tab"),
            $listReel = $(".tabContentReel"),
            $tabContents = $(".tabContent"),
            reelSize = 1;
        // reelTransformPercent = 1;

        reelSize = $filterTabs.length;


        // reelTransformPercent = 100 / reelSize;



        $filterTabs.each(function (index, element) {
            $(element).click(function (event) {
                var _$thisTab = $(this),
                    _$thisTabContent = $("#" + _$thisTab.data("tabid")),
                    maxTabHeight = ANGKOR.recalculateTabContent() || 1;

                event.preventDefault();
                $tabContents.hide();

                _$thisTabContent.fadeIn(1000);

                if (!(_$thisTab.hasClass("active"))) {
                    $filterTabs.add($tabContents).removeClass("active ");

                    _$thisTab.addClass("active");
                    _$thisTabContent.addClass("active");
                    // console.log(maxTabHeight);
                    _$thisTabContent.css("height", maxTabHeight + "px");
                }

                ANGKOR.bookingDynamicInputPadding();
            });
        })
    },
    recalculateTabContent: function () {
        var $tabContents = $(".tabContent"),
            maxTabHeight = 1;

        $tabContents.each(function (index, tabContent) {
            maxTabHeight = maxTabHeight < $(tabContent).innerHeight()
                ? $(tabContent).innerHeight()
                : maxTabHeight;
        });

        return maxTabHeight;
    },
    bookingSelection: function () {
        var x, i, j, selElmnt, a, b, c;
        /*look for any elements with the class "customSelectWrap":*/
        x = document.getElementsByClassName("customSelectWrap");
        for (i = 0; i < x.length; i++) {
            selElmnt = x[i].getElementsByTagName("select")[0];
            /*for each element, create a new DIV that will act as the selected item:*/
            a = document.createElement("DIV");
            a.setAttribute("class", "select-selected");
            a.innerHTML = selElmnt.options[selElmnt.selectedIndex].innerHTML;
            if (selElmnt.selectedIndex === 0) {
                x[i].classList.add("withoutValue");
            } else {
                x[i].classList.remove("withoutValue");
            }
            x[i].appendChild(a);
            /*for each element, create a new DIV that will contain the option list:*/
            b = document.createElement("DIV");
            b.setAttribute("class", "select-items select-hide");
            for (j = 1; j < selElmnt.length; j++) {
                /*for each option in the original select element,
                create a new DIV that will act as an option item:*/
                c = document.createElement("DIV");
                c.innerHTML = selElmnt.options[j].innerHTML;
                c.addEventListener("click", function (e) {
                    /*when an item is clicked, update the original select box,
                    and the selected item:*/
                    var y, i, k, s, h;
                    s = this.parentNode.parentNode.getElementsByTagName("select")[0];
                    h = this.parentNode.previousSibling;
                    for (i = 0; i < s.length; i++) {
                        if (s.options[i].innerHTML == this.innerHTML) {
                            s.selectedIndex = i;
                            h.innerHTML = this.innerHTML;
                            if (i === 0) {
                                // x[i].classList.add("withoutValue");
                                this.parentNode.parentNode.classList.add("withoutValue");

                            } else {
                                // x[i].classList.remove("withoutValue");
                                this.parentNode.parentNode.classList.remove("withoutValue");
                            }

                            y = this.parentNode.getElementsByClassName("same-as-selected");
                            for (k = 0; k < y.length; k++) {
                                y[k].removeAttribute("class");
                            }
                            this.setAttribute("class", "same-as-selected");
                            break;
                        }
                    }
                    h.click();
                });
                b.appendChild(c);
            }
            x[i].appendChild(b);
            a.addEventListener("click", function (e) {
                /*when the select box is clicked, close any other select boxes,
                and open/close the current select box:*/
                e.stopPropagation();
                closeAllSelect(this);
                this.nextSibling.classList.toggle("select-hide");
                this.classList.toggle("select-arrow-active");
            });
        }
        function closeAllSelect(elmnt) {
            /*a function that will close all select boxes in the document,
            except the current select box:*/
            var x, y, i, arrNo = [];
            x = document.getElementsByClassName("select-items");
            y = document.getElementsByClassName("select-selected");
            for (i = 0; i < y.length; i++) {
                if (elmnt == y) {
                    arrNo.push(i);
                } else {
                    y[i].classList.remove("select-arrow-active");
                }
            }
            for (i = 0; i < x.length; i++) {
                if (arrNo.indexOf(i)) {
                    x[i].classList.add("select-hide");
                }
            }
        }
        /*if the user clicks anywhere outside the select box,
        then close all select boxes:*/
        document.addEventListener("click", closeAllSelect);
    },
    bookingDatePicker: function () {
        var dateFormat = "mm/dd/yy";
        // from = $("#from")
        //     .datepicker({
        //         defaultDate: "+1w",
        //         changeMonth: true,
        //         numberOfMonths: 3
        //     })
        //     .on("change", function () {
        //         to.datepicker("option", "minDate", getDate(this));
        //     }),
        // to = $("#to").datepicker({
        //     defaultDate: "+1w",
        //     changeMonth: true,
        //     numberOfMonths: 3
        // })
        //     .on("change", function () {
        //         from.datepicker("option", "maxDate", getDate(this));
        //     });

        function getDate(element) {
            var date;
            try {
                date = $.datepicker.parseDate(dateFormat, element.value);
            } catch (error) {
                date = null;
            }

            return date;
        }

        // 
        var from = $('#from').datetimepicker({
            format: 'd M, Y',
            onShow: function (ct) {
                this.setOptions({
                    maxDate: $('#to').val() ? $('#to').val() : false
                })
            },
            timepicker: false
        });

        var to = $('#to').datetimepicker({
            format: 'd M, Y',
            onShow: function (ct) {
                this.setOptions({
                    minDate: $('#from').val() ? $('#from').val() : false
                })
            },
            timepicker: false
        });


        // $(".dropBtn").toggle(
        //     function () {
        //         var $dropContent = $(this).next();
        //         $(".dropBtn").removeClass("show");
        //         $(".dropContent").removeClass("show");
        //         $(this).addClass("show");
        //         $dropContent.addClass("show");
        //     },
        //     function () {
        //         var $dropContent = $(this).next();
        //         $(".dropBtn").removeClass("show");
        //         $(".dropContent").removeClass("show");
        //     }
        // )

        var from = $('#hotelbookingdeparture').datetimepicker({
            format: 'd M, Y',
            onShow: function (ct) {
                this.setOptions({
                    maxDate: $('#hotelbookingreturn').val() ? $('#hotelbookingreturn').val() : false
                })
            },
            timepicker: false
        });

        var to = $('#hotelbookingreturn').datetimepicker({
            format: 'd M, Y',
            onShow: function (ct) {
                this.setOptions({
                    minDate: $('#hotelbookingdeparture').val() ? $('#hotelbookingdeparture').val() : false
                })
            },
            timepicker: false
        });
    },
    bookingDynamicInputPadding: function () {
        var labelsToPad = $(".js_dynamicPadding");
        labelsToPad.each(function (index, element) {
            var input = $(this).next("input"),
                thisDimensions = $(this).getHiddenDimensions() || {};
            var thisWidth = thisDimensions.innerWidth || 0;
            input.css("padding-left", thisWidth + 20);
        });
    },
    sectionTitleAnimation: function () {
        var testTitle = document.querySelectorAll(".sectionTitle .heading");
        for (var i = 0; i < testTitle.length; i++) {
            temp = new tempAni(testTitle[i]);
            temp.init();
        }
    }
}

function tempAni(tt) {

    var tl = new TimelineMax();
    var n = new SplitText(tt);
    // console.log(n.charElements);
    var o = n.charElements;

    var rAnimation = function () {
        tl
            .staggerTo(o, 1, {
                // x: "0",
                autoAlpha: 0,
                ease: Expo.easeOut
            }, .15)
            .staggerTo(o, 1.5, {
                // x: "100",
                autoAlpha: 1,
                delay: 1,
                ease: Expo.easeInOut,
                onComplete: function () {
                    tl.restart();
                }
            });
    }

    var lAnimation = function () {
        n.revert();
        tl.kill();
    }

    return {
        init: rAnimation,
        kill: lAnimation
    }

}



// 
// function scrollHelperAnimation() {
//     var e = new TimelineMax
//       , t = new TimelineMax
//       , a = new TimelineMax
//       , n = new SplitText(".helper-scroll .text",{
//         type: "chars",
//         charsClass: "helper-chars"
//     })
//       , o = n.chars
//       , i = document.querySelectorAll(".helper-chars");
//     var r = function() {
//         i.forEach(function(e) {
//             var t = document.createElement("div");
//             e.parentNode.insertBefore(t, e);
//             t.appendChild(e)
//         });
//         TweenMax.set(".helper-scroll", {
//             top: window.innerHeight - document.querySelector(".helper-scroll").offsetHeight * 1.5,
//             autoAlpha: 1
//         });
//         e.to(".helper-scroll .arrow-wrapper", 2, {
//             y: "0%",
//             ease: Expo.easeOut
//         }).to(".helper-scroll .arrow-wrapper", 1, {
//             y: "100%",
//             delay: 1,
//             ease: Expo.easeInOut,
//             onComplete: function() {
//                 e.restart();
//                 t.restart();
//                 a.restart()
//             }
//         });
//         a.staggerTo(o, 1, {
//             y: "0%",
//             ease: Expo.easeOut
//         }, .15).to(o, 1.5, {
//             y: "100%",
//             delay: 1,
//             ease: Expo.easeInOut
//         });
//         t.to(".helper-scroll .arrow-svg", 2, {
//             scaleX: 1,
//             delay: .3,
//             ease: Expo.easeOut
//         }).to(".helper-scroll .arrow-svg", 1, {
//             scaleX: 0,
//             delay: .4,
//             ease: Expo.easeInOut
//         })
//     };
//     var l = function() {
//         n.revert();
//         TweenMax.set([".helper-scroll .arrow-wrapper", ".helper-scroll .arrow-svg"], {
//             clearProps: "all"
//         });
//         e.kill();
//         t.kill();
//         a.kill();
//         TweenMax.set(".helper-scroll", {
//             clearProps: "top",
//             autoAlpha: 0
//         })
//     };
//     return {
//         init: r,
//         kill: l
//     }
// }
//
var pHome = {
    init: function () {
        console.log("<pHome> => INIT!");
        //pHome.galleryInit();
        $(window).on('resize', pHome.onResize);
        pHome.onResize();

        GLoader.loadScripts([
            path_resource + "js/plugins/slick/slick.min.js",
            path_resource + "js/plugins/slick/slick.css",
        ], function () {
            pHome.initSliders();
            ANGKOR.init();
            PRELOADER.hide();
        });
        this.search.bar();

        // PRELOADER.hide();
    },

    onResize: function (e) {
        // do your fucking resizing
        console.log("Browser size: " + window.innerWidth + "x" + window.innerHeight);
        // alert("Browser size: " + window.innerWidth + "x" + window.innerHeight);
        ANGKOR.bookingDynamicInputPadding();
    },
    galleryInit: function () {
        //console.log("INIT GALLERY");
    },
    initSliders: function () {
        var bannerSlider;
        var tl = new TimelineMax({ paused: true });
        var $firstBannerSlide = $(".hero:first-child"),
            $elAirplane = $firstBannerSlide.find(".elAirplane"),
            $elWind = $firstBannerSlide.find(".elWind"),
            $originText = $firstBannerSlide.find(".originText"),
            $midText = $firstBannerSlide.find(".midText"),
            $destText = $firstBannerSlide.find(".destText"),
            $heroInfo = $(".hero:first-child .heroInfo > *");
        $(".twoSlider").slick({
            slidesToShow: 2,
            slideToScroll: 2,
            dots: true,
            arrows: false,
            infinite: false,
        });
        $(".fiveSlider").slick({
            slideToScroll: 1,
            slidesToShow: 5,
            dots: true,
            arrows: true,
            infinite: false,
        });

        // the banner 
        // console.group('BannerHero');
        // console.log('slick', slick);
        // console.groupEnd();

        TweenMax.set($elAirplane, { autoAlpha: 0, scale: 0.1, x: -300, y: 250 });
        TweenMax.set($elWind, { autoAlpha: 0, x: 100, y: -150 });
        TweenMax.set($originText, { autoAlpha: 0, y: 50 });
        TweenMax.set($midText, { autoAlpha: 0, scale: 0.1, x: 100 });
        TweenMax.set($destText, { autoAlpha: 0, y: -50 });
        TweenMax.set($heroInfo, { autoAlpha: 0, y: 100 });
        TweenMax.set("#booking", { autoAlpha: 0, y: -100 });

        tl
            .add("start", 0)
            .to($elAirplane, 1.2, { autoAlpha: 1, scale: 1, x: 0, y: 0, ease: Sine.easeOut }, "start")
            .to("#booking", 1.2, { autoAlpha: 1, y: 0, ease: Sine.easeOut }, "start")
            .add('end1', 1.1)
            .to($elWind, 0.5, { autoAlpha: 1, x: 0, y: 0 }, "end1")
            .to($midText, 0.5, { autoAlpha: 1, scale: 1, x: 0 }, "end1")
            .add("end3", 1.5)
            .to($originText, 0.5, { autoAlpha: 1, scale: 1, y: 0 }, "end3")
            .to($destText, 0.5, { autoAlpha: 1, scale: 1, y: 0 }, "end3")
            .staggerTo($heroInfo, 1, { autoAlpha: 1, y: 0 }, 0.25, "end1");

        $(".bannerHero").on("init", function (event, slick) {
            tl.play();
        })
        bannerSlider = $(".bannerHero").slick({
            slideToScroll: 1,
            slidesToShow: 1,
            dots: true,
            arrows: false,
            infinite: false,
        });

        bannerSlider.on("afterChange", function (event, slick, currentSlide, nextSlide) {

        });
    },
    uploadInitQuestionMark: function () {

        $("#btn-upload").on("click", function (e) {
            e.preventDefault();
            // Browse photo from library
            DUpload.browse();
            DUpload.onSelect = function (base64) {
                console.log(base64);
                var img = new Image();
                img.src = base64;
                img.style.width = "300px";
                img.style.height = "auto";
                $(".imgContainer")[0].appendChild(img);
            };
        });
    },
    search: {
        bar: function () {
            $(".js_navSearchBtn").on("touchstart click", function (e) {
                e.preventDefault();
                var e = ($("header .searchHolder").hasClass("visible"), $("header .searchHolder input").val());
                return "" != e && null != e
                    ? (pHome.search.html($("header .searchHolder input").val()), !1)
                    : ($(".header .searchHolder input").focus(), void $("header .searchHolder").toggleClass("visible"));
            }),
                $(".searchHolder form").on("submit", function (e) {
                    e.preventDefault();
                    pHome.search.html($("header .searchHolder input").val())
                })
        },
        html: function (e) {
            $("header .searchHolder").removeClass("visible");
            console.log("submitting..");
        }
    }
}


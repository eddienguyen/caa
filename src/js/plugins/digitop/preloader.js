/* PRELOADER - version 1.0
- Description: Show/hide default preloader
- Date: Aug 16, 2017
- Author: Goon Nguyen
================================================== */
var PRELOADER = {
    selector: document.getElementById("preloader"),
    $top: $("#bgTop"),
    $bot: $("#bgBot"),
    $progressBar: $("#preloader .progressBar"),
    $airplane: $("#preloader .airplane"),
    loaded: 0,
    speed: 600,
    timer: null,
    init: function () {
        this.resetProgress();
    },
    resetProgress: function (callback) {
        this.loaded = 0;
        this.$progressBar.css({
            left: 0
        });
        this.$airplane.css({
            left: "-64px"
        });
        // $(this.selector).removeClass("open");
        // this.$top.removeClass("open");
        // this.$bot.removeClass("open");
        clearTimeout(PRELOADER.timer);

        if (callback) callback();
    },
    show: function (callback) {
        // $("#preloader").removeClass("helper-hide");
        $(PRELOADER.selector).removeClass("open");
        PRELOADER.$top.removeClass("open");
        PRELOADER.$bot.removeClass("open");
        PRELOADER.update();
    },
    update: function (callback) {
        PRELOADER.timer = setTimeout(function () {
            PRELOADER.loaded += Math.random() * 10;
            PRELOADER.loaded = parseFloat(PRELOADER.loaded.toFixed(1));

            if (PRELOADER.loaded >= 100) {

                PRELOADER.loaded = 100;
                PRELOADER.hide();

            } else {
                PRELOADER.speed = Math.floor(Math.random() * 1000);
                PRELOADER.update();
            }

            PRELOADER.$progressBar.css({
                width: PRELOADER.loaded + "%"
            });
            PRELOADER.$airplane.css({
                left: PRELOADER.loaded + "%"
            });
        }, PRELOADER.speed);
    },
    hide: function (callback) {
        // $("#preloader").addClass("helper-hide");
        PRELOADER.loaded = 100;
        $(PRELOADER.selector).addClass("open");
        PRELOADER.$top.addClass("open");
        PRELOADER.$bot.addClass("open");
        setTimeout(function () {
            PRELOADER.resetProgress();
        }, 2000);
    }
}

PRELOADER.init();
PRELOADER.show();